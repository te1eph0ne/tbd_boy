typedef union reg {
  unsigned short reg;
  struct {
    unsigned char hi;
    unsigned char lo;
  };
} reg;
